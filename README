################################################################
README for gewocs
################################################################
Version 1.
Updated 2013-05-03
=================================================================
Overview
=================================================================
gewocs is a simple Java webapp, which
	1. gets calculation parameters from the Web user interface,
	2. passes the parameters as environment variables to gewocs.ncl,
	3. executes gewocs.ncl, and
	4. returns the produced files to the user.

It is a Maven Eclipse project.

It uses:
	Spring
	Struts (action MVC design pattern)
	DAO design pattern
	Apache Commons Exec to execute NCL script
	OpenLayers for the Map tool interface
	ImageMagic to overlay UNSW Logo to images
	
gewocs.ncl requires NCAR Command Language (NCL) to be installed 
on the system.

=================================================================
NCAR Command Language (NCL) installation
=================================================================
 
This Web application gets data from DAO, which uses NCL.

- For local development, NCL is installed in its default location.
- For production, NCL is installed in /share/apps/ncl.
 
The location is specified in applicationContext.xml and
applicationContext-test.xml.
 		   	
=================================================================
Testing NCL from command line on the production/demo servers
=================================================================
NCL is installed as a module in /share/apps/ncl

So, to use it from shell:

	module add ncl

and

	vi .bash_profile
	(insert)
	export PATH=$PATH:/share/apps/ncl/6.0.0/bin #??? Can I specify somewhere else more standard?
	export NCARG_ROOT=/share/apps/ncl/6.0.0
	export DISPLAY=:0.0
	unlimited

Add install ~/.hluresfile

Logout and login via NoMachine client to tornado with a gnome session.

Then, ran the tests:

	module add ncl
	ng4ex gsun01n
	ncl gsun01n.ncl 
	

=================================================================
JUnit test and generating WAR
=================================================================
Generating WAR with Maven
- The code generation from the source is affected by the JUnit test codes.
- If a test fails, the code/package is not generated.
- If it is desired, create the package by 
	mvn -Dmaven.test.skip=true install	
- applicationContext-test uses the same environments as the production
- Before the project can be built with tests, all environments must be set up		
 
Setting up production and test environments
- NCL executables are installed at e.g. /share/apps/ncl/6.0.0
- NCL scripts are placed at /scratch/climdex/NCLScripts/gewocs/
- NCL outputs are placed at /scratch/climdex/NCLScripts/gewocs/results
- UNSW logo is placed at /scratch/climdex/NCLScripts/gewocs/
- Input data locations are: 
	/scratch/climdex/ghcndex
	/scratch/climdex/hadex

All these locations can be specified in applicationContext.xml
and applicationContext-test.xml.

If applicationContext.xml is modified at runtime, it requires webapp to
restart to load it.
   		
To generate gewocs.war for production,
1. Replace the content of applicationContext.xml with content of applicationContext-tornado.xml
2. Build gewocs.war with maven, e.g.,
 	
	mvn -Dmaven.test.skip=true install
 		
This is because currently the JUnit tests are not working and the problem
has not been fixed.
 	
=================================================================
Deploying WAR on production
=================================================================
It does not seem that using the manager interface is the best way.

Stop tomcat and manually replace WAR, then restart the server.

=================================================================
gewocs.ncl and files produced
=================================================================

gewocs webapp uses /scratch/climdex/NCLScripts/gewocs/gewocs.ncl

NCLScripts directory can be cloned out from the climdex git repository.

e.g.,
https://bitbucket.org/climdex/ncl
or
$ cd /scratch/climdex
$ git clone https://bitbucket.org/climdex/ncl.git NCLScripts.git

Make sure that the NCL directory has read/write permission for
the webapp user account (tomcat-climdex on tornado.ccrc.unsw.edu.au).

NCL scripts cannot return the images as in-memory data to Java.

For this reason, the images had to be written on the file system
so that Java webapp can read them and provide them to the end user
as file download or viewed as images.

Currently, gewocs.ncl writes files in 
/scratch/climdex/NCLScripts/gewocs/results.

These files are overwritten if the request uses the same name.

There may be thousands of these files produced and they may need
to be cleaned up from time to time by e.g. cron job.

=================================================================
Modification of certain constants for the application
=================================================================

Certain constants can be changed in WEB-INF/classes/applicationContext.xml.

This gives a way to change those values without modifying them in the code.

After modifying values in the applicationContext, it needs reloading to
force the Webapp to read them.



