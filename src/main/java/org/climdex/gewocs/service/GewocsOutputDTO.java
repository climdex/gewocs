/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.service;

import java.io.InputStream;

/**
 * Gewocs request bean
 * 
 * @author yoichi2
 * 
 */
public class GewocsOutputDTO
{
	private String dataSet;
	private String climateIndex;
	private String startYear;
	private String endYear;
	private String season;
	private String minLat;
	private String maxLat;
	private String minLon;
	private String maxLon;
	private String outputType;
	private String outputFormat;

	private InputStream fileInputStream;
	private String contentType;
	private String fileName;

	public String getDataSet()
	{
		return dataSet;
	}

	public void setDataSet(String dataSet)
	{
		this.dataSet = dataSet;
	}

	public String getClimateIndex()
	{
		return climateIndex;
	}

	public void setClimateIndex(String climateIndex)
	{
		this.climateIndex = climateIndex;
	}

	public String getStartYear()
	{
		return startYear;
	}

	public void setStartYear(String startYear)
	{
		this.startYear = startYear;
	}

	public String getEndYear()
	{
		return endYear;
	}

	public void setEndYear(String endYear)
	{
		this.endYear = endYear;
	}

	public String getSeason()
	{
		return season;
	}

	public void setSeason(String season)
	{
		this.season = season;
	}

	public String getMinLat()
	{
		return minLat;
	}

	public void setMinLat(String minLat)
	{
		this.minLat = minLat;
	}

	public String getMaxLat()
	{
		return maxLat;
	}

	public void setMaxLat(String maxLat)
	{
		this.maxLat = maxLat;
	}

	public String getMinLon()
	{
		return minLon;
	}

	public void setMinLon(String minLon)
	{
		this.minLon = minLon;
	}

	public String getMaxLon()
	{
		return maxLon;
	}

	public void setMaxLon(String maxLon)
	{
		this.maxLon = maxLon;
	}

	public String getOutputType()
	{
		return outputType;
	}

	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}

	public String getOutputFormat()
	{
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat)
	{
		this.outputFormat = outputFormat;
	}

	public InputStream getFileInputStream()
	{
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream)
	{
		this.fileInputStream = fileInputStream;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

}
