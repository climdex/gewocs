package org.climdex.gewocs.service.dao;

public interface DataFilePathFinder
{

	abstract String getFilePath(String dataSet, String climateIndex);

}
