package org.climdex.gewocs.service.dao;

import org.climdex.gewocs.model.DatasetOptions;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A trivial class to return data file path for a dataset by replacing the
 * ${INDEX} in the file name. TODO: Use File.separator instead of "/" for
 * Windows.
 * 
 * @author yoichi
 * 
 */
public class DataFilePathFinderImpl implements DataFilePathFinder
{
	// These values are replaced in applicationContext.xml
	@Autowired
	private DatasetOptions datasetOptions;

	@Autowired
	private String dataSetDataFilePath;

	public String getFilePath(String dataSet, String indexName)
	{
		String filePathStr = null;
		filePathStr = this.datasetOptions.getDataFilePathMap().get(dataSet).trim();
		String realfilePathStr = filePathStr.replaceAll("\\$\\{INDEX\\}",
				indexName);
		this.dataSetDataFilePath = realfilePathStr;
		return realfilePathStr;
	}

	public DatasetOptions getDatasetOptions()
	{
		return datasetOptions;
	}

	public void setDatasetOptions(DatasetOptions datasetOptions)
	{
		this.datasetOptions = datasetOptions;
	}

	public String getDataSetDataFilePath()
	{
		return dataSetDataFilePath;
	}

	public void setDataSetDataFilePath(String dataSetDataFilePath)
	{
		this.dataSetDataFilePath = dataSetDataFilePath;
	}
}
