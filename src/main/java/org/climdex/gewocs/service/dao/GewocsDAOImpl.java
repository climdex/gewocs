/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.service.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.climdex.gewocs.service.GewocsInputDTO;
import org.climdex.gewocs.service.GewocsOutputDTO;
import org.climdex.gewocs.service.dao.ncl.NclCommonsExec;
import org.climdex.gewocs.service.dao.ncl.NclCommonsExecException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO class to produce requested climate extreme index
 * 
 * The main method returns GewocsOutputDTO
 * 
 * Prerequisites: NCL, Xvfb
 * 
 * The class specifies where the output file will be, but has no knowledge as to
 * where the input files are or where the NCL files are. The parameters are set
 * by applictionContext.xml.
 * 
 * @see README for more details.
 */
public class GewocsDAOImpl implements GewocsDAO
{
	private static final Logger logger = Logger.getLogger(GewocsDAOImpl.class);

	/**
	 * Constants TODO: Definitions should be moved to a central place
	 */
	private static final String DELIM = File.separator;

	public static final String OUTPUT_AVERAGE_MAP = "AverageMap";
	public static final String OUTPUT_TREND_MAP = "TrendMap";
	public static final String OUTPUT_TIME_SERIES = "TimeSeries";
	public static final String OUTPUT_RAW_DATA = "RawData";

	public static final String OUTPUT_ASCII = "ASCII";
	public static final String OUTPUT_PNG = "PNG";
	public static final String OUTPUT_NETCDF = "netCDF";

	public static final String MIME_PNG = "image/png";
	public static final String MIME_NETCDF = "application/x-netcdf";
	public static final String MIME_ASCII = "test/plain";

	public static final String SUFFIX_PNG = "png";
	public static final String SUFFIX_NETCDF = "nc";
	public static final String SUFFIX_ASCII = "txt";

	/**
	 * Default values for production.
	 * 
	 * These values are defined here, because they are used by both Java and
	 * NCL.
	 * 
	 * TODO: Use File.separator instead of "/"
	 */
	private static final String NCL_SCRIPT_PATH = "/scratch/climdex/NCLScripts/gewocs/plot4.ncl";
	private static final String GEWOCS_RESULT_DIR = "/scratch/climdex/NCLScripts/gewocs/results";

	/**
	 * These allows applicationContext.xml to override the default values
	 * Particularly useful for test XXX: When these values are set by
	 * applicationContext.xml, some strings are too long and XML end tag may be
	 * wrapped. When this happens, an extra space is added to the value. That is
	 * the reason why .trim() should be used.
	 */
	@Autowired
	private DataFilePathFinder dataFilePathFinder = null;
	private String nclScriptPath = NCL_SCRIPT_PATH;
	private String gewocsResultDir = GEWOCS_RESULT_DIR;
	@Autowired
	private NclCommonsExec nclCommonsExec = null;

	/**
	 * A service interface method. It runs NCL script that creates the results
	 * into a file. Then, it reads the file and returns it as FileInputStream
	 * 
	 * @throws DAOException
	 */
	@Override
	public GewocsOutputDTO getGewocsOutput(GewocsInputDTO gewocsInputDTO)
			throws DAOException
	{
		// GewocsNclDTO needs slightly different properties
		GewocsNclDTO gewocsNclDTO = makeNclDTO(gewocsInputDTO);

		// outFilePath includes the directory name where the file is going to be
		// saved.
		String fileName = gewocsNclDTO.getFileName();
		String outFilePath = this.gewocsResultDir.trim() + DELIM + fileName;

		File outputFile = new File(outFilePath);
		FileInputStream fis = null;
		GewocsOutputDTO gewocsOutputDTO = null;
		try
		{
			if (!outputFile.exists())
			{
				try
				{
					this.nclCommonsExec.execute(gewocsNclDTO);
				}
				catch (NclCommonsExecException e)
				{
					throw new DAOException(e);
				}
			}
			// GewocsOutputDTO is the object to be returned to service
			// which requires slightly different properties
			gewocsOutputDTO = new GewocsOutputDTO();
			BeanUtils.copyProperties(gewocsNclDTO, gewocsOutputDTO);

			// If the file exists, hopefully it is readable by the time user
			// gets to download it.
			// For example, if another thread tries to read it at the same
			// time???
			// This may throw FileNotFound exception
			// outputFile.setReadOnly();
			fis = new FileInputStream(outputFile);
			gewocsOutputDTO.setFileInputStream(fis);
		}
		catch (FileNotFoundException e)
		{
			throw new DAOException(e);
		}

		return gewocsOutputDTO;
	}

	/**
	 * A utility method for getGewocsOutput()
	 * 
	 * @param gewocsInputDTO
	 * @return
	 * @throws DAOException
	 */
	protected GewocsNclDTO makeNclDTO(GewocsInputDTO gewocsInputDTO)
			throws DAOException
	{
		String dataSet = gewocsInputDTO.getDataSet();
		String climateIndex = gewocsInputDTO.getClimateIndex();
		String startYear = gewocsInputDTO.getStartYear();
		String endYear = gewocsInputDTO.getEndYear();
		String season = gewocsInputDTO.getSeason();
		String minLat = gewocsInputDTO.getMinLat();
		String maxLat = gewocsInputDTO.getMaxLat();
		String minLon = gewocsInputDTO.getMinLon();
		String maxLon = gewocsInputDTO.getMaxLon();
		String outputType = gewocsInputDTO.getOutputType();
		String outputFormat = gewocsInputDTO.getOutputFormat();

		logger.debug("dataSet= " + dataSet);
		logger.debug("climateIndex= " + climateIndex);
		logger.debug("startYear= " + startYear);
		logger.debug("endYear= " + endYear);
		logger.debug("season= " + season);
		logger.debug("minLat= " + minLat);
		logger.debug("maxLat= " + maxLat);
		logger.debug("minLon= " + minLon);
		logger.debug("maxLon= " + maxLon);
		logger.debug("outputType= " + outputType);
		logger.debug("outputFormat= " + outputFormat);

		// GewocsNclDTO is an object to pass to the NCLExec
		GewocsNclDTO gewocsNclDTO = new GewocsNclDTO();
		BeanUtils.copyProperties(gewocsInputDTO, gewocsNclDTO);

		String suffix = "";
		String contentType = "";
		if (OUTPUT_ASCII.equals(outputFormat))
		{
			suffix = SUFFIX_ASCII;
			contentType = MIME_ASCII;
		}
		else
			if (OUTPUT_NETCDF.equals(outputFormat))
			{
				suffix = SUFFIX_NETCDF;
				contentType = MIME_NETCDF;
			}
			else
				if (OUTPUT_PNG.equals(outputFormat))
				{
					suffix = SUFFIX_PNG;
					contentType = MIME_PNG;
				}
				else
					throw new DAOException(outputType + ": unknown outputType");

		gewocsNclDTO.setContentType(contentType);

		String fileName = outputType + "_" + dataSet + "_" + climateIndex + "_"
				+ startYear + "-" + endYear + "_" + season + "_from" + minLat
				+ "to" + maxLat + "_from" + minLon + "to" + maxLon + "."
				+ suffix;

		gewocsNclDTO.setFileName(fileName);

		String outFilePath = this.gewocsResultDir.trim() + DELIM + fileName;
		gewocsNclDTO.setOutFilePath(outFilePath);

		String dataFilePath = this.dataFilePathFinder.getFilePath(dataSet,
				climateIndex);
		gewocsNclDTO.setDataFilePath(dataFilePath);

		gewocsNclDTO.setScriptPath(this.nclScriptPath);

		logger.debug("contentType= " + contentType);
		logger.debug("fileName= " + fileName);
		logger.debug("outFilePath= " + outFilePath);
		logger.debug("dataFilePath= " + dataFilePath);
		logger.debug("nclScriptPath= " + nclScriptPath);

		return gewocsNclDTO;
	}

	public void setDataFilePathFinder(DataFilePathFinder dataFilePathFinder)
	{
		this.dataFilePathFinder = dataFilePathFinder;
	}

	public DataFilePathFinder getDataFilePathFinder()
	{
		return dataFilePathFinder;
	}

	public String getNclScriptPath()
	{
		return nclScriptPath;
	}

	public void setNclScriptPath(String nclScriptPath)
	{
		this.nclScriptPath = nclScriptPath;
	}

	public String getGewocsResultDir()
	{
		return gewocsResultDir;
	}

	public void setGewocsResultDir(String gewocsResultDir)
	{
		this.gewocsResultDir = gewocsResultDir;
	}

	public NclCommonsExec getNclCommonsExec()
	{
		return nclCommonsExec;
	}

	public void setNclCommonsExec(NclCommonsExec nclCommonsExec)
	{
		this.nclCommonsExec = nclCommonsExec;
	}
}
