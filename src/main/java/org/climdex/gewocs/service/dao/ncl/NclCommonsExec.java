/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.service.dao.ncl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.climdex.gewocs.service.dao.GewocsNclDTO;
import org.springframework.beans.factory.annotation.Autowired;

// import org.apache.commons.exec.environment.EnvironmentUtils;
/**
 * A class to run exec with org.apache.commons.exec utility classes. The code
 * library is somewhat old: Last Published: 02 November 2010, Version:
 * 1.1.1-SNAPSHOT
 * 
 * The class does not know where the input data is located.
 * 
 * Each thread of this script must have different set of env to allow different
 * calculation by the NCL script. This is yet to be tested for multi-thread
 * situation.
 * 
 * XXX: TIME_OUT_IN_SEC has been changed from 60 to 120 for HADEX2.
 * 
 * @link http://commons.apache.org/exec/
 * 
 * @author yoichi2
 * 
 */
public class NclCommonsExec
{
	private static final Logger logger = Logger.getLogger(NclCommonsExec.class);

	@Autowired
	private String NCARG_ROOT = "/usr/local/";
	@Autowired
	private String NCARG_LIB = "/usr/local/lib";
	@Autowired
	private String NCARG_NCARG = "/usr/local/lib/ncarg";
	@Autowired
	private String NCARG_FONTCAPS = "/usr/local/lib/ncarg/fontcaps";
	@Autowired
	private String NCARG_GRAPHCAPS = "/usr/local/lib/ncarg/graphcaps";
	@Autowired
	private String NCARG_DATABASE = "/usr/local/lib/ncarg/database";
	@Autowired
	private String NCL_CMD = NCARG_ROOT + "/bin/ncl";
	@Autowired
	private String NCARG_USRRESFILE = "/scratch/climdex/NCLScripts/gewocs/hluresfile";
	@Autowired
	private String GEWOCS_LOGO_PATH = "/scratch/climdex/NCLScripts/gewocs/logo_unsw_small.png";
	@Autowired
	private String CONVERT_CMD = "/opt/local/bin/convert";

	private static final int SECOND = 1000;
	private static final int TIME_OUT_IN_SEC = 120;

	// private static final boolean NOT_QUOATED = false;

	public void execute(GewocsNclDTO nclDto) throws NclCommonsExecException
	{
		String dataSet = nclDto.getDataSet();
		String climateIndex = nclDto.getClimateIndex();
		String startYear = nclDto.getStartYear();
		String endYear = nclDto.getEndYear();
		String season = nclDto.getSeason();
		String minLat = nclDto.getMinLat();
		String maxLat = nclDto.getMaxLat();
		String minLon = nclDto.getMinLon();
		String maxLon = nclDto.getMaxLon();
		String outputType = nclDto.getOutputType();
		String outputFormat = nclDto.getOutputFormat();
		String dataFilePath = nclDto.getDataFilePath();
		String outFilePath = nclDto.getOutFilePath();
		String scriptPath = nclDto.getScriptPath();

		logger.debug(dataSet);
		logger.debug(climateIndex);
		logger.debug(startYear);
		logger.debug(endYear);
		logger.debug(season);
		logger.debug(minLat);
		logger.debug(maxLat);
		logger.debug(minLon);
		logger.debug(maxLon);
		logger.debug(outputType);
		logger.debug(outputFormat);
		logger.debug(dataFilePath);
		logger.debug(outFilePath);
		logger.debug(scriptPath);

		// String dataSetString = "'dataSet=\"" + dataSet + "\"'";
		// String climateIndexStr = "'climateIndex=\"" + climateIndex + "\"'";
		// String startYearStr = "startYear=" + startYear;
		// String endYearStr = "endYear=" + endYear;
		// String seasonStr = "'season=\"" + season + "\"'";
		// String regionStr = "'region=\"" + region + "\"'";
		// String outputTypeStr = "'outputType=\"" + outputType + "\"'";
		// String filePathStr = "'filePath=\"" + filePath + "\"'";

		CommandLine cmdLine = new CommandLine(NCL_CMD);
		/*
		 * Setting args does not work well, because NCL has a peculiar
		 * definition about its "variable types" "text" or "numerical" on the
		 * command line.
		 */
		// cmdLine.addArgument(climateIndexStr, NOT_QUOATED);
		// cmdLine.addArgument(startYearStr);
		// cmdLine.addArgument(endYearStr);
		// cmdLine.addArgument(seasonStr, NOT_QUOATED);
		// cmdLine.addArgument(outputTypeStr, NOT_QUOATED);
		/* this applies system dependent path separator properly */
		cmdLine.addArgument("${file}");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("file", new File(scriptPath));
		cmdLine.setSubstitutionMap(map);

		System.out.println(cmdLine.toString());
		// String[] arguments = cmdLine.getArguments();
		// for (String argument: arguments)
		// System.out.println(argument);

		Map<String, String> environment = new HashMap<String, String>();
		environment.put("NCARG_ROOT", NCARG_ROOT);
		environment.put("NCARG_LIB", NCARG_LIB);
		environment.put("NCARG_NCARG", NCARG_NCARG);
		environment.put("NCARG_FONTCAPS", NCARG_FONTCAPS);
		environment.put("NCARG_GRAPHCAPS", NCARG_GRAPHCAPS);
		environment.put("NCARG_DATABASE", NCARG_DATABASE);
		environment.put("NCARG_USRRESFILE", NCARG_USRRESFILE);
		environment.put("GEWOCS_LOGO_PATH", GEWOCS_LOGO_PATH);
		environment.put("CONVERT_CMD", CONVERT_CMD);

		logger.debug("NCARG_ROOT=" + NCARG_ROOT);
		logger.debug("NCARG_LIB=" + NCARG_LIB);
		logger.debug("NCARG_NCARG=" + NCARG_NCARG);
		logger.debug("NCARG_FONTCAPS=" + NCARG_FONTCAPS);
		logger.debug("NCARG_GRAPHCAPS=" + NCARG_GRAPHCAPS);
		logger.debug("NCARG_DATABASE=" + NCARG_DATABASE);
		logger.debug("NCARG_USRRESFILE=" + NCARG_USRRESFILE);
		logger.debug("GEWOCS_LOGO_PATH=" + GEWOCS_LOGO_PATH);
		logger.debug("CONVERT_CMD=" + CONVERT_CMD);

		// These do not work as command line args, but work as env
		// output file path has to match with what Java has used
		environment.put("GEWOCS_DATAFILE_PATH", dataFilePath);
		environment.put("GEWOCS_OUTFILE_PATH", outFilePath);
		environment.put("GEWOCS_DATA_SET", dataSet);
		environment.put("GEWOCS_CLIMATE_INDEX", climateIndex);
		environment.put("GEWOCS_SEASON", season);
		environment.put("GEWOCS_MIN_LAT", minLat);
		environment.put("GEWOCS_MAX_LAT", maxLat);
		environment.put("GEWOCS_MIN_LON", minLon);
		environment.put("GEWOCS_MAX_LON", maxLon);
		environment.put("GEWOCS_OUTPUT_TYPE", outputType);
		environment.put("GEWOCS_OUTPUT_FORMAT", outputFormat);
		// These can be passed as command line args
		// but NCL code looks explicit if they are defined as env
		environment.put("GEWOCS_START_YEAR", String.valueOf(startYear));
		environment.put("GEWOCS_END_YEAR", String.valueOf(endYear));

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		ExecuteWatchdog watchdog = new ExecuteWatchdog(TIME_OUT_IN_SEC * SECOND);
		Executor executor = new DefaultExecutor();
		// executor.setExitValue(1);
		executor.setWatchdog(watchdog);
		ByteArrayOutputStream stdout = null;
		PumpStreamHandler psh = null;
		try
		{
			stdout = new ByteArrayOutputStream();
			psh = new PumpStreamHandler(stdout);
			executor.setStreamHandler(psh);

			// This call starts the process
			executor.execute(cmdLine, environment, resultHandler);
			// This call blocks until the process finishes
			// and the ResultHandler is called back.
			resultHandler.waitFor();
			// The process must have ended
			String stdoutStr = stdout.toString();
			logger.info(stdoutStr);
			int exitValue = resultHandler.getExitValue();
			if (exitValue != 0)
			{
				String errMsg = "Process exited with: " + exitValue;
				errMsg += "\nstdout: " + stdoutStr;
				logger.warn(errMsg);
				throw new NclCommonsExecException(errMsg);
			}
		}
		catch (IOException e)
		{
			logger.warn(e.getMessage());
			throw new NclCommonsExecException(e);
		}
		catch (InterruptedException e)
		{
			logger.warn(e.getMessage());
			throw new NclCommonsExecException(e);
		}
		finally
		{
			if (stdout != null)
			{
				try
				{
					stdout.close();
				}
				catch (IOException e)
				{
					;
				}
				stdout = null;
			}
		}
	}

	public String getNCARG_ROOT()
	{
		return NCARG_ROOT;
	}

	public void setNCARG_ROOT(String nCARG_ROOT)
	{
		NCARG_ROOT = nCARG_ROOT;
	}

	public String getNCARG_LIB()
	{
		return NCARG_LIB;
	}

	public void setNCARG_LIB(String nCARG_LIB)
	{
		NCARG_LIB = nCARG_LIB;
	}

	public String getNCARG_NCARG()
	{
		return NCARG_NCARG;
	}

	public void setNCARG_NCARG(String nCARG_NCARG)
	{
		NCARG_NCARG = nCARG_NCARG;
	}

	public String getNCARG_FONTCAPS()
	{
		return NCARG_FONTCAPS;
	}

	public void setNCARG_FONTCAPS(String nCARG_FONTCAPS)
	{
		NCARG_FONTCAPS = nCARG_FONTCAPS;
	}

	public String getNCARG_GRAPHCAPS()
	{
		return NCARG_GRAPHCAPS;
	}

	public void setNCARG_GRAPHCAPS(String nCARG_GRAPHCAPS)
	{
		NCARG_GRAPHCAPS = nCARG_GRAPHCAPS;
	}

	public String getNCARG_DATABASE()
	{
		return NCARG_DATABASE;
	}

	public void setNCARG_DATABASE(String nCARG_DATABASE)
	{
		NCARG_DATABASE = nCARG_DATABASE;
	}

	public String getNCL_CMD()
	{
		return NCL_CMD;
	}

	public void setNCL_CMD(String nCL_CMD)
	{
		NCL_CMD = nCL_CMD;
	}

	public String getNCARG_USRRESFILE()
	{
		return NCARG_USRRESFILE;
	}

	public void setNCARG_USRRESFILE(String nCARG_USRRESFILE)
	{
		NCARG_USRRESFILE = nCARG_USRRESFILE;
	}

	public String getGEWOCS_LOGO_PATH()
	{
		return GEWOCS_LOGO_PATH;
	}

	public void setGEWOCS_LOGO_PATH(String gEWOCS_LOGO_PATH)
	{
		GEWOCS_LOGO_PATH = gEWOCS_LOGO_PATH;
	}

	public String getCONVERT_CMD()
	{
		return CONVERT_CMD;
	}

	public void setCONVERT_CMD(String cONVERT_CMD)
	{
		CONVERT_CMD = cONVERT_CMD;
	}
}
