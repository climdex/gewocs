/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.service;

import org.climdex.gewocs.service.dao.DAOException;
import org.climdex.gewocs.service.dao.GewocsDAO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Service Interface implementation. It uses DAO.
 * 
 * @author yoichi
 */
public class GewocsServiceImpl implements GewocsService
{
	@Autowired
	private GewocsDAO gewocsDAO = null;

	/**
	 * Method to retrieve TrendMap, AverageMap or Trend as PNG, ASCII or NetCDF
	 */
	@Override
	public GewocsOutputDTO getGewocsOutput(GewocsInputDTO gewocsInputDTO)
			throws ServiceException
	{
		GewocsOutputDTO gewocsOutputDTO;
		try
		{
			gewocsOutputDTO = gewocsDAO.getGewocsOutput(gewocsInputDTO);
		}
		catch (DAOException e)
		{
			throw new ServiceException(e);
		}

		return gewocsOutputDTO;
	}

	public GewocsDAO getGewocsDAO()
	{
		return this.gewocsDAO;
	}

	public void setGewocsDAO(GewocsDAO gewocsDAO)
	{
		this.gewocsDAO = gewocsDAO;
	}
}
