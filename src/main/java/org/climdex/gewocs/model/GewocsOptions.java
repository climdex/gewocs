/*
 * Copyright 2010 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.model;

import java.util.ArrayList;
import java.util.List;

/**
 * GewocsRequest options for the HTML FORM SELECT. These are default options.
 * Actual options values would change on the actual JSP. This is also sent back
 * and forth in the session scope, so it can be used to keep the current values
 * selected by the end user.
 * 
 * This is the way Spring parses the Java list and create SELECT options on JSP
 * pages.
 * 
 * TODO: Add capability for defaultSelected, selected or disabled.
 * 
 * TODO: Replace initXXX() methods with setXXX()/getXXX(). This allows applicationContext.xml to set values.
 * TODO: Methods can be added to allow the JSP page to get values directly from this bean.
 * 
 * @author yoichi
 */
public class GewocsOptions {
	private List<KeyValueElement> dataSetList;
	private List<KeyValueElement> climateIndexList;
	private List<String> startYearList;
	private List<String> endYearList;
	// This is for GHCNDEX which is default
	private int yearListStart = Globals.DEFAULT_START_YEAR;
	// This for GHCNDEX which is default
	private int yearListEnd = Globals.DEFAULT_END_YEAR;
	private List<KeyValueElement> seasonList;
	private List<KeyValueElement> regionList;
	private List<KeyValueElement> outputTypeList;
	private List<KeyValueElement> outputFormatList;

	/**
	 * Constructor with initializing values TODO: These can be done as
	 * initializers
	 */
	public GewocsOptions() {
		initDataSetList();
		initClimateIndexList();
		initStartYearList();
		initEndYearList();
		initSeasonList();
		initRegionList();
		initOutputTypeList();
		initOutputFormatList();
	}

	private void initDataSetList() {
		dataSetList = new ArrayList<KeyValueElement>();

		dataSetList.add(new KeyValueElement("GHCNDEX", "GHCNDEX"));
		// dataSetList.add(new KeyValueElement("HADEX1", "HADEX1"));
		dataSetList.add(new KeyValueElement("HADEX2", "HADEX2"));
	}

	private void initClimateIndexList() {
		climateIndexList = new ArrayList<KeyValueElement>();
		climateIndexList.add(new KeyValueElement("FD", "Frost days (FD)"));
		climateIndexList.add(new KeyValueElement("SU", "Summer days (SU)"));
		climateIndexList.add(new KeyValueElement("ID", "Ice days (ID)"));
		climateIndexList.add(new KeyValueElement("TR", "Tropical nights (TR)"));
		climateIndexList.add(new KeyValueElement("GSL",
				"Growing season length (GSL)"));
		climateIndexList.add(new KeyValueElement("TXx", "Max Tmax (TXx)"));
		climateIndexList.add(new KeyValueElement("TNx", "Max Tmin (TNx)"));
		climateIndexList.add(new KeyValueElement("TXn", "Min Tmax (TXn)"));
		climateIndexList.add(new KeyValueElement("TNn", "Min Tmin (TNn)"));
		climateIndexList
				.add(new KeyValueElement("TN10p", "Cool nights (TN10p)"));
		climateIndexList.add(new KeyValueElement("TX10p", "Cool days (TX10p)"));
		climateIndexList
				.add(new KeyValueElement("TN90p", "Warm nights (TN90p)"));
		climateIndexList.add(new KeyValueElement("TX90p", "Warm days (TX90p)"));
		climateIndexList.add(new KeyValueElement("WSDI",
				"Warm spell duration indicator (WSDI)"));
		climateIndexList.add(new KeyValueElement("CSDI",
				"Cold spell duration indicator (CSDI)"));
		climateIndexList.add(new KeyValueElement("DTR",
				"Diurnal temparature range (DTR)"));
		climateIndexList.add(new KeyValueElement("Rx1day",
				"Max 1-day precipitation amount (Rx1day)"));
		climateIndexList.add(new KeyValueElement("Rx5day",
				"Max 5-day precipitation amount (Rx5day)"));
		climateIndexList.add(new KeyValueElement("SDII",
				"Simple daily intensity index (SDII)"));
		climateIndexList.add(new KeyValueElement("R10mm",
				"Number of heavy precipitation days (R10mm)"));
		climateIndexList.add(new KeyValueElement("R20mm",
				"Number of very heavy precipitation days (R20mm)"));
		// climateIndexList.add(new KeyValueElement("Rnnmm",
		// "Number of days above nn mm (Rnnmm)"));
		climateIndexList.add(new KeyValueElement("CDD",
				"Consecutive dry days (CDD)"));
		climateIndexList.add(new KeyValueElement("CWD",
				"Consecutive wet days (CWD)"));
		climateIndexList
				.add(new KeyValueElement("R95p", "Very wet days (R95p)"));
		climateIndexList.add(new KeyValueElement("R99p",
				"Extremely wet days (R99p)"));
		climateIndexList.add(new KeyValueElement("PRCPTOT",
				"Annual total wet-day precipitation (PRCPTOT)"));
	}

	/*
	 * This sets the initial endYearList for GHCNDEX. Alternatively, we can do
	 * this only on the JSP page because now the list has to be changed when
	 * HADEX2 is selected
	 */
	private void initStartYearList() {
		startYearList = new ArrayList<String>();
		for (int i = yearListStart; i <= yearListEnd; i++) {
			startYearList.add(String.valueOf(i)); // new String instance
		}
	}

	/*
	 * This sets the initial startYearList for GHCNDEX. Alternatively, we can do
	 * this only on the JSP page because now the list has to be changed when
	 * HADEX2 is selected
	 */
	private void initEndYearList() {
		endYearList = new ArrayList<String>();
		for (int i = yearListStart; i <= yearListEnd; i++) {
			endYearList.add(String.valueOf(i)); // new String instance //
												// created
		}
	}

	private void initSeasonList() {
		seasonList = new ArrayList<KeyValueElement>();
		seasonList.add(new KeyValueElement("ANN", "Annual"));
		seasonList.add(new KeyValueElement("DJF", "DJF"));
		seasonList.add(new KeyValueElement("MAM", "MAM"));
		seasonList.add(new KeyValueElement("JJA", "JJA"));
		seasonList.add(new KeyValueElement("SON", "SON"));
		seasonList.add(new KeyValueElement("JAN", "January"));
		seasonList.add(new KeyValueElement("FEB", "February"));
		seasonList.add(new KeyValueElement("MAR", "March"));
		seasonList.add(new KeyValueElement("APR", "April"));
		seasonList.add(new KeyValueElement("MAY", "May"));
		seasonList.add(new KeyValueElement("JUN", "June"));
		seasonList.add(new KeyValueElement("JUL", "July"));
		seasonList.add(new KeyValueElement("AUG", "August"));
		seasonList.add(new KeyValueElement("SEP", "September"));
		seasonList.add(new KeyValueElement("OCT", "October"));
		seasonList.add(new KeyValueElement("NOV", "November"));
		seasonList.add(new KeyValueElement("DEC", "December"));
	}

	private void initRegionList() {
		regionList = new ArrayList<KeyValueElement>();
		regionList.add(new KeyValueElement("GLOBE", "Globe"));
		regionList.add(new KeyValueElement("AFRICA", "Africa"));
		regionList.add(new KeyValueElement("ASIA", "Asia"));
		regionList.add(new KeyValueElement("EUROPE", "Europe"));
		regionList.add(new KeyValueElement("OCEANIA", "Oceania"));
		regionList.add(new KeyValueElement("NAMERICA", "North America"));
		regionList.add(new KeyValueElement("SAMERICA", "South America"));
	}

	private void initOutputTypeList() {
		outputTypeList = new ArrayList<KeyValueElement>();
		outputTypeList.add(new KeyValueElement("AverageMap", "Average Map"));
		outputTypeList
				.add(new KeyValueElement("TrendMap","Trend Map"));
		outputTypeList.add(new KeyValueElement("TimeSeries", "Time Series"));
		outputTypeList.add(new KeyValueElement("RawData", "Raw Data"));
	}

	private void initOutputFormatList() {
		outputFormatList = new ArrayList<KeyValueElement>();
		outputFormatList.add(new KeyValueElement("PNG", "Plot (PNG)"));
		outputFormatList.add(new KeyValueElement("netCDF", "netCDF"));
		outputFormatList.add(new KeyValueElement("ASCII", "ASCII"));
	}

	public List<KeyValueElement> getDataSetList() {
		return dataSetList;
	}

	public void setDataSetList(List<KeyValueElement> dataSetList) {
		this.dataSetList = dataSetList;
	}

	public List<KeyValueElement> getClimateIndexList() {
		return climateIndexList;
	}

	public void setClimateIndexList(List<KeyValueElement> climateIndexList) {
		this.climateIndexList = climateIndexList;
	}

	public int getYearListStart() {
		return yearListStart;
	}

	public void setYearListStart(int yearListStart) {
		this.yearListStart = yearListStart;
	}

	public int getYearListEnd() {
		return yearListEnd;
	}

	public void setYearListEnd(int yearListEnd) {
		this.yearListEnd = yearListEnd;
	}

	public List<String> getStartYearList() {
		return startYearList;
	}

	public void setStartYearList(List<String> startYearList) {
		this.startYearList = startYearList;
	}

	public List<String> getEndYearList() {
		return endYearList;
	}

	public void setEndYearList(List<String> endYearList) {
		this.endYearList = endYearList;
	}

	public List<KeyValueElement> getSeasonList() {
		return seasonList;
	}

	public void setSeasonList(List<KeyValueElement> seasonList) {
		this.seasonList = seasonList;
	}

	public List<KeyValueElement> getRegionList() {
		return regionList;
	}

	public void setRegionList(List<KeyValueElement> regionList) {
		this.regionList = regionList;
	}

	public List<KeyValueElement> getOutputTypeList() {
		return outputTypeList;
	}

	public void setOutputTypeList(List<KeyValueElement> outputTypeList) {
		this.outputTypeList = outputTypeList;
	}

	public List<KeyValueElement> getOutputFormatList() {
		return outputFormatList;
	}

	public void setOutputFormatList(List<KeyValueElement> outputFormatList) {
		this.outputFormatList = outputFormatList;
	}
}
