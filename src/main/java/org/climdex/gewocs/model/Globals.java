package org.climdex.gewocs.model;

/**
 * Default values for GewocsRequest.
 * 
 * For the START_YEAR and END_YEAR, this sets default values for GHCNDEX.
 * Although GHCNDEX currently has data only up to Oct. 2012, the END_YEAR is set
 * to the current year. HADEX2 is for 1901 to 2010. Although this can be set
 * here and used in the JSP page, the period for HADEX2 is fixed and the HADEX2
 * values are hard-coded in the checkDataSet() script.
 * 
 * TODO: DEFAULT_END_YEAR should be set by applicationContext.xml so that it can
 * be changed.
 * 
 * TODO: HADEX2 values should be set by applicationContext.xml when application
 * has multiple dataset capability properly implemented.
 * 
 * @author yoichi
 * 
 */
public class Globals {
	public static final String DEFAULT_DATA_SET = "GHCNDEX";
	public static final String DEFAULT_CLIMATE_INDEX = "TXx";
	/** @deprecated Now this should be set by DatasetOptions */
	public static int DEFAULT_START_YEAR = 0;
	/** @deprecated Now this should be set by DatasetOptions */
	public static int DEFAULT_END_YEAR = 0;
	public static final String DEFAULT_SEASON = "ANN";
	public static final String DEFAULT_MIN_LAT = "-90";
	public static final String DEFAULT_MAX_LAT = "90";
	public static final String DEFAULT_MIN_LON = "-180";
	public static final String DEFAULT_MAX_LON = "180";
	public static final String DEFAULT_OUTPUT_TYPE = "TrendMap";
	public static final String DEFAULT_OUTPUT_FORMAT = "PNG";
}
