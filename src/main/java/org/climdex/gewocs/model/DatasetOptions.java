package org.climdex.gewocs.model;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class holds a pair of startYear and endYear values for different
 * Datasets. The bean is passed with HTTP Request and JavaScript can query the
 * bean to retrieve the values. Actually values are set by
 * applicationContext.xml in order to allow admin to change/set values without
 * changing the code.
 * 
 * For beans to be set by Spring, it has to use map syntax in
 * applicationContext.xml.
 * 
 * JavaScript should query it as an associative array in JSP (not from script
 * file).
 * 
 * <pre>
 * <code>
 *    eval('var dataSetJSArray = ${datasetOptions.startYearJSArray}');
 *    var ghcndexStartYear = dataSetJSArray.GHCNDEX;
 *    alert(ghcndexStartYear);
 *  </code>
 * </pre>
 * 
 * @author yoichi
 * 
 */
public class DatasetOptions
{
	/**
	 * This map is set as a Java object but used to produce a JSArray
	 */
	@Autowired
	private Map<String, String> startYearMap = null;

	/**
	 * This map is set as a Java object but used to produce a JSArray
	 */
@Autowired
	private Map<String, String> endYearMap = null;
	
/**
 * This map is used only as Java object
 */
@Autowired
private Map<String, String> dataFilePathMap;

/**
 * This map is used only as Java object
 */
@Autowired
private Map<String, String> archiveFileUrlMap;

	/**
	 * Test constructor
	 */
	public DatasetOptions()
	{
	}

	/**
	 * This method is used by Spring to set a map from applicationContext.xml
	 * 
	 * @param startYearMap
	 */
	public void setStartYearMap(Map<String, String> startYearMap)
	{
		this.startYearMap = startYearMap;
	}

	/**
	 * This method does not work in JavaScript as expected.
	 * 
	 * <pre>
	 * var startYearMap = ${datasetOptions.startYearMap};
	 * </pre>
	 * 
	 * It will produce:
	 * 
	 * <pre>
	 * var startYearMap = { GHCNDEX = 1951, HADEX2 = 1901 };
	 * </pre>
	 * 
	 * which has no meaning in JavaScript. That is why getXyzMapJSArray()
	 * is needed.
	 * 
	 * @return
	 */
	public Map<String, String> getStartYearMap()
	{
		return startYearMap;
	}

	/**
	 * This method is used by Spring to set a map from applicationContext.xml
	 * 
	 * @param endYearMap
	 */
	public void setEndYearMap(Map<String, String> endYearMap)
	{
		this.endYearMap = endYearMap;
	}

	public Map<String, String> getEndYearMap()
	{
		return endYearMap;
	}

	public Map<String, String> getDataFilePathMap()
	{
		return dataFilePathMap;
	}

	public void setDataFilePathMap(Map<String, String> dataFilePathMap)
	{
		this.dataFilePathMap = dataFilePathMap;
	}

	public Map<String, String> getArchiveFileUrlMap()
	{
		return archiveFileUrlMap;
	}

	public void setArchiveFileUrlMap(Map<String, String> archiveFileUrlMap)
	{
		this.archiveFileUrlMap = archiveFileUrlMap;
	}

	/**
	 * This method is used to get the map as an associative array in JavaScript.
	 * 
	 * <pre>
	 * <code>
	 * eval('var startYearJSArray = ${datasetOptions.startYearJSArray};');
	 * alert(startYearJSArray.HADEX2);
	 * </code>
	 * </pre>
	 * 
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public String getStartYearJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String startYearJSString = mapper.writeValueAsString(startYearMap);
		return startYearJSString;
	}

	/**
	 * This method is used to get the map as an associative array in JavaScript
	 * 
	 * <pre>
	 * eval('var endYearJSArray = ${datasetOptions.endYearJSArray};');
	 * alert(endYearJSArray.HADEX2);
	 * </pre>
	 * 
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public String getEndYearJSArray() throws JsonGenerationException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		String endYearJSString = mapper.writeValueAsString(endYearMap);
		return endYearJSString;
	}
}
