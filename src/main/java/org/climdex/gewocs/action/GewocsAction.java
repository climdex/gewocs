/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.action;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.climdex.gewocs.model.DatasetOptions;
import org.climdex.gewocs.model.GewocsOptions;
import org.climdex.gewocs.model.GewocsRequest;
import org.climdex.gewocs.model.Seasonality;
import org.climdex.gewocs.service.GewocsInputDTO;
import org.climdex.gewocs.service.GewocsOutputDTO;
import org.climdex.gewocs.service.GewocsService;
import org.climdex.gewocs.service.ServiceException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

/**
 * XXX: @Autowired is disabled in struts.properties and applicationContext.xml
 * It is used to indicate that those fields are wired with Spring beans or used
 * by Struts. However, some accessors do not work if @autowired is removed.
 * 
 * @author yoichi2
 * 
 *         TODO: Double submission handling should be implemented
 * 
 */
public class GewocsAction extends ActionSupport
{
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(GewocsAction.class);

	@Autowired
	private GewocsRequest gewocsRequest;
	@Autowired
	private DatasetOptions datasetOptions;
	@Autowired
	private GewocsOptions gewocsOptions;
	@Autowired
	private Seasonality seasonality;
	@Autowired
	private GewocsService gewocsService;

	/**
	 * For file attachment (handled by Struts2)
	 */
	@Autowired
	private InputStream fileInputStream = null;
	@Autowired
	private String contentType;
	@Autowired
	private String fileName;

	/**
	 * Method to obtain the gridded index as either TrendMap, AverageMap, Plot,
	 * ACSII or NetCDF
	 * 
	 * @return String
	 */
	@Override
	public String execute()
	{
		String dataSet = this.gewocsRequest.getDataSet();
		String climateIndex = this.gewocsRequest.getClimateIndex();
		String startYear = this.gewocsRequest.getStartYear();
		String endYear = this.gewocsRequest.getEndYear();
		String season = this.gewocsRequest.getSeason();
		String minLat = this.gewocsRequest.getMinLat();
		String maxLat = this.gewocsRequest.getMaxLat();
		String minLon = this.gewocsRequest.getMinLon();
		String maxLon = this.gewocsRequest.getMaxLon();
		String outputType = this.gewocsRequest.getOutputType();
		String outputFormat = this.gewocsRequest.getOutputType();

		logger.debug("dataSet= " + dataSet);
		logger.debug("climateIndex= " + climateIndex);
		logger.debug("startYear= " + startYear);
		logger.debug("endYear= " + endYear);
		logger.debug("season= " + season);
		logger.debug("minLat= " + minLat);
		logger.debug("maxLat= " + maxLat);
		logger.debug("minLon= " + minLon);
		logger.debug("maxLon= " + maxLon);
		logger.debug("outputType= " + outputType);
		logger.debug("outputFormat= " + outputFormat);

		GewocsInputDTO gewocsRequestInputDTO = new GewocsInputDTO();
		BeanUtils.copyProperties(this.gewocsRequest, gewocsRequestInputDTO);
		GewocsOutputDTO gewocsRequestOutputDTO = null;
		try
		{
			gewocsRequestOutputDTO = gewocsService
					.getGewocsOutput(gewocsRequestInputDTO);
			this.fileName = gewocsRequestOutputDTO.getFileName();
			this.fileInputStream = gewocsRequestOutputDTO.getFileInputStream();
			this.contentType = gewocsRequestOutputDTO.getContentType();
		}
		catch (ServiceException e)
		{
			if (this.fileInputStream != null) try
			{
				this.fileInputStream.close();
			}
			catch (IOException e1)
			{
				;
			}
			this.fileInputStream = null;
			String errMsg = e.getLocalizedMessage();
			addActionError(errMsg);
			return ERROR;
		}

		return NONE;
	}

	/**
	 * The method to initialize the beans for the first time
	 * 
	 * @return String
	 */
	public String display()
	{
		return SUCCESS;
	}

	public GewocsRequest getGewocsRequest()
	{
		return gewocsRequest;
	}

	public void setGewocsRequest(GewocsRequest gewocsRequest)
	{
		this.gewocsRequest = gewocsRequest;
	}

	public DatasetOptions getDatasetOptions()
	{
		return datasetOptions;
	}

	public void setDatasetOptions(DatasetOptions datasetOptions)
	{
		this.datasetOptions = datasetOptions;
	}

	public GewocsOptions getGewocsOptions()
	{
		return gewocsOptions;
	}

	public void setGewocsOptions(GewocsOptions gewocsOptions)
	{
		this.gewocsOptions = gewocsOptions;
	}

	public Seasonality getSeasonality()
	{
		return seasonality;
	}

	public void setSeasonality(Seasonality seasonality)
	{
		this.seasonality = seasonality;
	}

	public GewocsService getGewocsService()
	{
		return gewocsService;
	}

	public void setGewocsService(GewocsService gewocsService)
	{
		this.gewocsService = gewocsService;
	}

	public InputStream getFileInputStream()
	{
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream)
	{
		this.fileInputStream = fileInputStream;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
}