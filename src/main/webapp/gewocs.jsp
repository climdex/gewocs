<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>GEWOCS</title>
<meta http-equiv="content-type"
	content="application/xhtml+xml; charset=UTF-8" />
<meta name="description"
	content="GEWOCS: GHCNDaily-based gridded Extreme Weather Observation Climate Statics by CLIMDEX Project Group" />
<meta name="keywords"
	content="gewocs,online download,climdex project,GHCNDEX,GHCN-Daily historical data,gridded,climate,change,trend map,trendmap,time series,timeseries,climate extremes indices,precipitation,temparature,etccdi,climate change detection" />

<s:head />

<link rel="stylesheet" href="styles/gewocsStyle.css" type="text/css" />
<link rel="stylesheet" href="styles/gewocsHeader.css" type="text/css" />
<link rel="stylesheet" href="styles/gewocsTabs.css" type="text/css" />
<link rel="stylesheet" href="styles/gewocsForm.css" type="text/css" />

<script src="/gewocs/scripts/test.js" type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkDataSet.js"
	type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkClimateIndex.js"
	type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkYears.js" type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkLatLon.js" type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkSeason.js" type="text/javascript"></script>
<script src="/gewocs/scripts/test_checkOutput.js" type="text/javascript"></script>
<script src="/gewocs/scripts/test_submit.js" type="text/javascript"></script>
<script src="/gewocs/scripts/gewocs_map.js" type="text/javascript"></script>

<style type="text/css">
#coordinates_fields {
	background-color: lightgreen;
}

#coordinates_fields {
	width: auto !important;
	background-color: lightgreen !important;
}

#coordinates_fields td {
	background-color: lightgreen;
}

#coordinates_fields label {
	background-color: lightgreen !important;
	width: 42px;
	float: left;
	margin-right: 3px;
	text-align: right;
	float: left;
}

.map_button {
	display: inline;
	position: relative;
	float: none;
	background-image: url("images/map_australia_up.png");
	background-color: transparent; /* make the button transparent */
	background-repeat: no-repeat;
	/* make the background image appear only once */
	background-position: center;
	height: 78px;
	width: 90px;
	border: solid; /* assuming we don't want any borders */
	border-width: 1px; /* assuming we don't want any borders */
	cursor: pointer;
	/* make the cursor like hovering over an <a> element */
	text-align: center;
	vertical-align: middle;
	/* align the text vertically centered */
}

.map_button:hover:after {
	background: #333;
	background: rgba(0, 0, 0, .8);
	border-radius: 5px;
	bottom: 26px;
	color: #fff;
	content: attr(title);
	left: 20%;
	padding: 5px 15px;
	position: absolute;
	z-index: 98;
	width: 220px;
}
</style>

<script>
	/* This script contains Struts2 action scope varaible reference. 
	 It does not work in script files but it works on JSP pages */
	var startYearJSArrayStr = '${datasetOptions.startYearJSArray}';
	var endYearJSArrayStr = '${datasetOptions.endYearJSArray}';

	eval('var startYearJSArray = ' + startYearJSArrayStr + ";");
	eval('var endYearJSArray = ${datasetOptions.endYearJSArray};');
</script>

</head>

<body id="bd" onload="initDataSet();matchSeasonToCilmateIndex();">
	<s:head />

	<s:form id="gewocsFormId" name="gewocsForm" action="gewocs.action"
		theme="simple" validate="false" accept-charset="UTF-8">

		<s:token />

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.dataSet" />
			<s:select key="gewocsRequest.dataSet"
				list="gewocsOptions.dataSetList" listKey="key" listValue="value"
				onchange="return checkDataSet(this)" />
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.climateIndex" />
			<s:select key="gewocsRequest.climateIndex"
				list="gewocsOptions.climateIndexList" listKey="key"
				listValue="value" onchange="return checkClimateIndex(this)" />
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.startYear" />
			<s:select key="gewocsRequest.startYear"
				list="gewocsOptions.startYearList"
				onchange="return checkStartYear(this)" />
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.endYear" />
			<s:select key="gewocsRequest.endYear"
				list="gewocsOptions.endYearList"
				onchange="return checkEndYear(this)" />
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.season" />
			<s:select key="gewocsRequest.season" list="gewocsOptions.seasonList"
				listKey="key" listValue="value" onchange="return checkSeason(this)" />
		</div>

		<div class="fieldsWrap">
			<label>Area</label>
			<div id="coordinates">

				<table id="coordinates_fields">
					<tr>
						<td colspan="3"><b>Enter coordinates in the fields below
								or use Map tool to select area. </b></td>
					</tr>
					<tr>
						<td colspan="3">Longitude: -180° &lt; x &lt; 180°, Latitude:
							-90° &lt; y &lt; 90°</td>
					</tr>
					<tr>
						<td></td>
						<td style="float: none !important; text-align: center !important;">
							<!--<s:label key="label.gewocsRequest.maxLat" />--> top<br /> <s:textfield
								key="gewocsRequest.maxLat" size="6"
								onchange="return checkMaxLat(this)" required="true" /> °
						</td>
						<td></td>
					</tr>
					<tr>
						<td style="float: right !important; text-align: right !important;">
							<!--<s:label key="label.gewocsRequest.minLon" />-->
							left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
							<s:textfield key="gewocsRequest.minLon" size="7"
								onchange="return checkLeftLon(this)" required="true" /> °
						</td>
						<td style="float: none !important; text-align: center !important">
							<input title="Use map tool to set coordinates" type="button"
							class="map_button" value="Map tool"
							alt="Use map tool to set coordinates" onclick="openMap(this)" />
						</td>
						<td style="float: left !important; text-align: left !important;">
							<!--<s:label key="label.gewocsRequest.maxLon" />-->
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;right<br /> <s:textfield
								key="gewocsRequest.maxLon" size="7"
								onchange="return checkRightLon(this)" required="true" /> °
						</td>
					</tr>
					<tr>
						<td></td>
						<td style="float: none !important; text-align: center !important;">
							<!--<s:label key="label.gewocsRequest.minLat" />--> <s:textfield
								key="gewocsRequest.minLat" size="6"
								onchange="return checkMinLat(this)" required="true" /> ° <br />bottom
						</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2"><input type="button"
							value="Set coordinates for entire globe"
							alt="Set coordinates for entire globe"
							onclick="selectGlobe(this)" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.outputType" />
			<div class="radioButtons">
				<table>
					<tr>
						<td><s:iterator value="gewocsOptions.outputTypeList">
								<s:radio name="gewocsRequest.outputType" list="#{key:value}"
									onchange="return checkOutputType(this)" />
								<br />
							</s:iterator></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="fieldsWrap">
			<s:label key="label.gewocsRequest.outputFormat" />
			<div class="radioButtons">
				<table>
					<tr>
						<td><s:iterator value="gewocsOptions.outputFormatList">
								<s:radio name="gewocsRequest.outputFormat" list="#{key:value}"
									onchange="return checkOutputFormat(this)" />
								<br />
							</s:iterator></td>
					</tr>
				</table>
			</div>
		</div>

		<div id="submit">
			<s:label value="Request" />
			<s:submit name="submit" cssClass="submitButton" value="Submit"
				action="gewocs.action" method="execute"
				onclick="this.form.action='gewocs.action'"
				onsubmit="return checkSubmit(this.form);" />
		</div>

		<p style="margin-left: 10px;">Note: Requesting ASCII may take a
			while for large data subset. Please be patient.</p>
	</s:form>

	<div>
		&nbsp;&nbsp;&nbsp;You can download all gridded data in compressed
		format by using links below: (60-80MB)<br />
		<div style="position: relative; left: 50pt">
			<!-- Ref: http://struts.apache.org/release/2.3.x/docs/iterator.html -->
			<!-- Ref: http://www.tutorialspoint.com/spring/spring_injecting_collection.htm -->
			<s:iterator status="status" value="datasetOptions.archiveFileUrlMap">
				<a href="<s:property value='value' />"><s:property value="key" /></a>
				<br />
			</s:iterator>
		</div>
	</div>

	<s:if
		test="hasFieldErrors() || hasActionMessages() || hasActionErrors()">
		<h6>
			<s:actionmessage id="errorMsg" />
			<s:fielderror id="errorMsg" />
			<s:actionerror id="errorMsg" />
		</h6>
	</s:if>

</body>
</html>
