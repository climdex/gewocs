/* CheckOutput (co) script */

function checkOutputType(outputTypeRadio)
{
	var outputType = outputTypeRadio.value;

	if ("RawData" == outputType)
		disablePlotForOutputFormat();
	else if ("TimeSeries" == outputType)
		disableNetCDFForOutputFormat();
	else
		enableOutputFormats();

	return true;
}

function checkOutputFormat(outputFormatRadio)
{
	return true;
}

function disablePlotForOutputFormat()
{
	var outputFormatPNG = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatPNG");
	var outputFormatNetDF = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatnetCDF");
	if (outputFormatPNG.checked == true)
	{
		outputFormatPNG.checked = false;
		outputFormatNetDF.checked = true;
	}
	outputFormatPNG.disabled = true;
	outputFormatNetDF.disabled = false;
}

function disableNetCDFForOutputFormat()
{
	var outputFormatPNG = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatPNG");
	var outputFormatNetDF = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatnetCDF");
	if (outputFormatNetDF.checked == true)
	{
		outputFormatPNG.checked = true;
		outputFormatNetDF.checked = false;
	}
	outputFormatPNG.disabled = false;
	outputFormatNetDF.disabled = true;
}

function enableOutputFormats()
{
	var outputFormatPNG = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatPNG");
	var outputFormatNetDF = document
			.getElementById("gewocsFormId_gewocsRequest_outputFormatnetCDF");
	outputFormatPNG.disabled = false;
	outputFormatNetDF.disabled = false;
}