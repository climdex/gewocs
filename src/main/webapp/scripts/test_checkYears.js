/**
 * functions to check startYear and endYear
 */

function checkStartYear(startYearSelect)
{
	var startYearIndex = startYearSelect.selectedIndex;
	var endYearSelect = startYearSelect.form.elements["gewocsRequest.endYear"];
	var endYearIndex = endYearSelect.selectedIndex;
	
	//alert("start="+startYearIndex+" end="+endYearIndex);

 	if (startYearIndex > endYearIndex)
	{
		alert("Start Year must be the same or earlier than End Year.");
		endYearSelect.selectedIndex = startYearIndex;
	}
	return true;
}

function checkEndYear(endYearSelect)
{
	var endYearIndex = endYearSelect.selectedIndex;
	var startYearSelect = endYearSelect.form.elements["gewocsRequest.startYear"];
	var startYearIndex = startYearSelect.selectedIndex;
	
	//alert("start="+startYearIndex+" end="+endYearIndex);
	
	if (startYearIndex > endYearIndex)
	{
		alert("End Year must be the same or later than Start Year");
		startYearSelect.selectedIndex = endYearIndex;
	}
	return true;
}
