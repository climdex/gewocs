
/**
 * Script to clear the error message
 */
function clearErrorMsg()
{
	var errorBox = document.getElementById("errorBox");
	if (errorBox != null)
	{
		errorBox.style = "color: white !important; background-color: #FFFFFF !important; border: #FFFFFF !important;";
		errorBox.innerHTML = "<ul style='color: white !important; background-color: #FFFFFF !important;'><li><span>no error</span></li></ul>";
	}
	;
}
