/**
 * Script to check the submit conditions
 */

var MIN_YEARS_TRENDMAP = 10;
var MIN_YEARS_TIMESERIES = 11;
var TREND_MAP = "TrendMap";
var TIME_SERIES = "TimeSeries";
var CLIMATE_INDEX_FIELD = "gewocsRequest.climateIndex";
var START_YEAR_FIELD = "gewocsRequest.startYear";
var END_YEAR_FIELD = "gewocsRequest.endYear";
var OUTPUT_TYPE_FIELD = "gewocsRequest.outputType";
var MIN_LAT_FIELD = "gewocsRequest.minLat";
var MAX_LAT_FIELD = "gewocsRequest.maxLat";
var MIN_LON_FIELD = "gewocsRequest.minLon";
var MAX_LON_FIELD = "gewocsRequest.maxLon";

function checkSubmit(theForm)
{
	clearErrorMsg();

	var climateIndex = theForm.elements[CLIMATE_INDEX_FIELD].value;
	if (climateIndex == -1)
	{
		alert("Please select an climate index");
		return false;
	}

	// Check period for Trend and Time Series
	var outputType = getOutputType(theForm);
	if (outputType == "")
	{
		alert("outputType cannot be empty");
		return false;
	}

	var startYearSelect = theForm.elements[START_YEAR_FIELD];
	var startYearIndex = startYearSelect.selectedIndex;
	var endYearSelect = theForm.elements[END_YEAR_FIELD];
	var endYearIndex = endYearSelect.selectedIndex;

	if (TREND_MAP == (outputType)
			&& ((endYearIndex - startYearIndex + 1) < MIN_YEARS_TRENDMAP))
	{
		alert("Period should be " + MIN_YEARS_TRENDMAP + " years or more.");
		return false;
	} else if (TIME_SERIES == (outputType)
			&& ((endYearIndex - startYearIndex + 1) < MIN_YEARS_TIMESERIES))
	{
		alert("Period should be " + MIN_YEARS_TIMESERIES + " years or more.");
		return false;
	}

	if (isCoordinatesGood(theForm) == false)
		return false;
	else
		return true;
}

function getOutputType(theForm)
{
	var radios = theForm.elements[OUTPUT_TYPE_FIELD];
	var length = radios.length;

	var outputType = "";
	for ( var i = 0; i < length; i++)
	{
		if (radios[i].checked == true)
		{
			outputType = radios[i].value;
			break;
		}
	}
	return outputType;
}

function isCoordinatesGood(theForm)
{
	var minLatE = theForm.elements[MIN_LAT_FIELD];
	var maxLatE = theForm.elements[MAX_LAT_FIELD];
	var minLonE = theForm.elements[MIN_LON_FIELD];
	var maxLonE = theForm.elements[MAX_LON_FIELD];
	var minLat = minLatE.value;
	var maxLat = maxLatE.value;
	var minLon = minLonE.value;
	var maxLon = maxLonE.value;

	/*
	 * empty check is necessary, because isNaN(EMPTY) returns true and
	 * Number(EMPTY) evaluates to 0, which is a valid integer. NOTE: !minLat is
	 * not exactly an empty check per se, but in this case only digits or EMPTY
	 * is expected
	 */

	if (!minLat)
	{
		alert("minLat is empty");
		return false;
	} else if (isNaN(minLat))
	{
		alert("minLat is not a number");
		return false;
	} else if (!maxLat)
	{
		alert("maxLat is empty");
		return false;
	} else if (isNaN(maxLat))
	{
		alert("maxLat is not a number");
		return false;
	} else if (!minLon)
	{
		alert("minLon is empty");
		return false;
	} else if (isNaN(minLon))
	{
		alert("minLon is not a number");
		return false;
	} else if (!maxLon)
	{
		alert("maxLon is empty");
		return false;
	} else if (isNaN(maxLon))
	{
		alert("maxLon is not a number");
		return false;
	} else
		return true;
}
