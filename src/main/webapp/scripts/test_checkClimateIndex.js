var indexToSeasonalList = new Array();
indexToSeasonalList = {
	"FD" : "annualOnly",
	"SU" : "annualOnly",
	"ID" : "annualOnly",
	"TR" : "annualOnly",
	"GSL" : "annualOnly",
	"TXx" : "seasonable",
	"TNx" : "seasonable",
	"TXn" : "seasonable",
	"TNn" : "seasonable",
	"TN10p" : "seasonable",
	"TX10p" : "seasonable",
	"TN90p" : "seasonable",
	"TX90p" : "seasonable",
	"WSDI" : "annualOnly",
	"CSDI" : "annualOnly",
	"DTR" : "seasonable",
	"Rx1day" : "seasonable",
	"Rx5day" : "seasonable",
	"SDII" : "annualOnly",
	"R10mm" : "annualOnly",
	"R20mm" : "annualOnly",
	"Rnn" : "annualOnly",
	"CDD" : "annualOnly",
	"CWD" : "annualOnly",
	"R95p" : "annualOnly",
	"R99p" : "annualOnly",
	"PRCPTOT" : "annualOnly"
};

var CI_DEFAULT_SELECTED = true;
var CI_NOT_DEFAULT_SELECTED = false;
var CI_SELECTED = true;
var CI_NOT_SELECTED = false;
function checkClimateIndex(indexSelect)
{
	// season selection differs between data sets.
	// e.g. HADEXs have only ANN and seasons, not monthly.
	// GHCNDEX currently does not have seasons.
	var indexValue = indexSelect.options[indexSelect.selectedIndex].value;
	
	var seasonSelect = indexSelect.form.elements["gewocsRequest.season"];
	var dataSetSelect = indexSelect.form.elements["gewocsRequest.dataSet"];
	var dataSetIndex = dataSetSelect.selectedIndex;
	var dataSet = dataSetSelect.options[dataSetIndex].value;

	var seasonability = indexToSeasonalList[indexValue];
	if (seasonability == "annualOnly")
	{
		seasonSelect.options.length = 0;
		seasonSelect.options.add(new Option("Annual", "ANN",
				CI_DEFAULT_SELECTED, CI_SELECTED));
		seasonSelect.value = "ANN";
	} else
	{
		seasonSelect.options.length = 0;
		seasonSelect.options[0] = new Option("Annual", "ANN",
				CI_DEFAULT_SELECTED, CI_SELECTED);

		if ("HADEX1" == dataSet)
		{
			seasonSelect.options[1] = new Option("DJF", "DJF", false, false);
			seasonSelect.options[2] = new Option("MAM", "MAM", false, false);
			seasonSelect.options[3] = new Option("JJA", "JJA", false, false);
			seasonSelect.options[4] = new Option("SON", "SON", false, false);
			// TODO: disabled = true should be set for months instead of not using them
		} else if ("GHCNDEX" == dataSet || "HADEX2" == dataSet)
		{
			// TODO: disabled = true should be set for seasons instead of not using them
			// TODO: SELECTED should copy the current value, if it
			// is applicable.
			seasonSelect.options[1] = new Option("January", "JAN",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[2] = new Option("February", "FEB",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[3] = new Option("March", "MAR",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[4] = new Option("April", "APR",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[5] = new Option("May", "MAY",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[6] = new Option("June", "JUN",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[7] = new Option("July", "JUL",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[8] = new Option("August", "AUG",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[9] = new Option("September", "SEP",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[10] = new Option("October", "OCT",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[11] = new Option("November", "NOV",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
			seasonSelect.options[12] = new Option("December", "DEC",
					CI_NOT_DEFAULT_SELECTED, CI_NOT_SELECTED);
		}
	}
	return true;
}

/**
 * document.onload script. This is probably not necessary, except if a wrong
 * selection was accidentally made by default setting etc.
 */
function matchSeasonToCilmateIndex()
{
	var climateIndexSelect = document.forms["gewocsForm"].elements["gewocsRequest.climateIndex"];
	var climateIndex = climateIndexSelect.value;
	if (climateIndex)
		checkClimateIndex(climateIndexSelect);
}
