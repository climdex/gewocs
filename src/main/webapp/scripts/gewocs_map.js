var gewocsMapWindow = null;
function openMap(field)
{
	var savedImage = field.style.backgroundImage;
	field.style.backgroundImage = "url('images/map_australia_down.png')";

	var url = "maptool.html";
	var name = "_blank";
	var specs = "menubar=0,location=0,toolbar=0,width=540,height=700";
	//var specs = "width=550, height=800";
	var replace = false;

	if (gewocsMapWindow == null || gewocsMapWindow.closed)
	{
		gewocsMapWindow = window.open(url, name, specs, replace);
	} else
	{
		gewocsMapWindow.location.href = url; // in case the user changed the href
		gewocsMapWindow.focus();
	}

	// TODO: copy values from the main form and draw a rectangle on the map,
	// but only if the values are not for the whole world.
	
	field.style.backgroundImage = savedImage;
}

function setCoordinates(minLon, maxLon, minLat, maxLat)
{
	var minLonE = document.getElementById("gewocsFormId_gewocsRequest_minLon");
	var maxLonE = document.getElementById("gewocsFormId_gewocsRequest_maxLon");
	var minLatE = document.getElementById("gewocsFormId_gewocsRequest_minLat");
	var maxLatE = document.getElementById("gewocsFormId_gewocsRequest_maxLat");
	minLonE.value = minLon;
	maxLonE.value = maxLon;
	minLatE.value = minLat;
	maxLatE.value = maxLat;
}
