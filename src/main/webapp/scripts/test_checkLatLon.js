/**
 * Scripts to check Lat and Lon values
 * minLat < maxLat and maxLat - minLax > DIFF
 * minLon < maxLon or maxLon < minLon and abs(maxLon - minLon) > DIFF
 * maxLon < minLon happens when the range spans over 180/-180 boundary
 */
/**
 * TODO: Add alerts for maxLat - minLat > DIFF
 * Some codes can be cleaned up more
 */

var DIFF = 5;

var LAT_MIN = -90;
var LAT_MIN_DIFF = -85;
var LAT_MAX = 90;
var LAT_MAX_DIFF = 85;

var LON_MIN = -180;
var LON_MAX = 180;

function checkMinLat(minLatE)
{
	var minLat = minLatE.value;
	var maxLatE = minLatE.form.elements["gewocsRequest.maxLat"];
	var maxLat = maxLatE.value;

	minLat = minLat.trim();
	if ("" === minLat)
	{
		alert("Please enter a number");
		minLatE.value = "";
		return true;
	}

	if (isNaN(minLat))
	{
		alert("Please enter a number");
		minLatE.value = "";
		return true;
	}

	var isMinLatChanged = false;
	if (Number(minLat) < Number(LAT_MIN))
	{
		minLat = LAT_MIN;
		isMinLatChanged = true;
	}
	if (Number(minLat) > Number(LAT_MAX_DIFF))
	{
		minLat = LAT_MAX_DIFF;
		maxLat = LAT_MAX;
		minLatE.value = minLat;
		maxLatE.value = maxLat;
		return true;
	}

	maxLat = maxLat.trim();
	if ("" === maxLat)
	{
		if (isMinLatChanged)
			minLatE.value = minLat;
		return true;
	}
	if (isNaN(maxLat))
	{
		maxLatE.value = "";
		if (isMinLatChanged)
			minLatE.value = minLat;
		return true;
	}
	var isMaxLatChanged = false;
	if (Number(maxLat) > Number(LAT_MAX))
	{
		maxLat = LAT_MAX;
		isMaxLatChanged = true;
	}
	// The code below ensures that maxLat > minLat.
	// Therefore, there is no need to check the lower limit
	if (Number(maxLat) - Number(minLat) < Number(DIFF))
	{
		maxLat = Number(minLat) + Number(DIFF);
		isMaxLatChanged = true;
		if (Number(maxLat) > Number(LAT_MAX))
		{
			maxLat = LAT_MAX;
			minLat = LAT_MAX_DIFF;
			minLatE.value = minLat;
			maxLatE.value = maxLat;
			return true;
		}
	}

	if (isMinLatChanged)
		minLatE.value = minLat;
	if (isMaxLatChanged)
		maxLatE.value = maxLat;
	return true;
}

function checkMaxLat(maxLatE)
{
	var maxLat = maxLatE.value;
	var minLatE = maxLatE.form.elements["gewocsRequest.minLat"];
	var minLat = minLatE.value;

	maxLat = maxLat.trim();
	if ("" === maxLat)
	{
		alert("Please enter a number");
		maxLatE.value = "";
		return true;
	}

	if (isNaN(maxLat))
	{
		alert("Please enter a number");
		maxLatE.value = "";
		return true;
	}
	if (Number(maxLat) < Number(LAT_MIN_DIFF))
	{
		alert("max Lat must be at least " + LAT_MAX);
		minLat = LAT_MIN;
		maxLat = LAT_MIN_DIFF;
		minLatE.value = minLat;
		maxLatE.value = maxLat;
		return true;
	}
	var isMaxLatChanged = false;
	if (Number(maxLat) > Number(LAT_MAX))
	{
		maxLat = LAT_MAX;
		isMaxLatChanged = true;
	}

	minLat = minLat.trim();
	if ("" === minLat)
	{
		if (isMaxLatChanged)
			maxLatE.value = maxLat;
		return true;
	}
	if (isNaN(minLat))
	{
		minLatE.value = "";
		if (isMaxLatChanged)
			maxLatE.value = maxLat;
		return true;
	}
	var isMinLatChanged = false;
	if (Number(minLat) < Number(LAT_MIN))
	{
		minLat = LAT_MIN;
		isMinLatChanged = true;
	}
	// The code below ensures that minLat < maxLat.
	// Therefore, there is no need to check the upper limit
	if (Number(maxLat) - Number(minLat) < Number(DIFF))
	{
		minLat = Number(maxLat) - Number(DIFF);
		isMinLatChanged = true;
		if (Number(minLat) < Number(LAT_MIN))
		{
			minLat = LAT_MIN;
			maxLat = -LAT_MIN_DIFF;
			maxLatE.value = maxLat;
			minLatE.value = minLat;
			return true;
		}
	}
	if (isMaxLatChanged)
		maxLatE.value = maxLat;
	if (isMinLatChanged)
		minLatE.value = minLat;
	return true;
}

function checkLeftLon(leftLonE)
{
	var leftLon = leftLonE.value.trim();
	var rightLonE = leftLonE.form.elements["gewocsRequest.maxLon"];
	var rightLon = rightLonE.value.trim();

	if ("" === leftLon)
	{
		alert("Please enter a number");
		leftLonE.value = "";
		return true;
	}
	if (isNaN(leftLon))
	{
		alert("Please enter a number");
		leftLonE.value = "";
		return true;
	}

	var leftLonNum = Number(leftLon);

	if (leftLonNum < LON_MIN)
	{
		alert("min Lon is " + LON_MIN);
		leftLonE.value = LON_MIN;
		rightLonE.value = LON_MIN + DIFF;
		return true;
	}

	if (leftLonNum > LON_MAX)
	{
		alert("max Lon is " + LON_MAX);
		leftLonE.value = LON_MAX;
		rightLonE.value = LON_MIN + DIFF;
		return true;
	}

	var rightLonNum = 0;
	if ("" === rightLon || isNaN(rightLon))
	{
		rightLonNum = leftLonNum + DIFF;
		if (LON_MAX < rightLonNum)
			rightLonNum = LON_MIN + (rightLonNum - LON_MAX);
	} else
		rightLonNum = Number(rightLon);

	var diff = 0;
	if (leftLonNum <= rightLonNum)
		diff = rightLonNum - leftLonNum;
	else if (rightLonNum < leftLonNum)
		diff = (rightLonNum - leftLonNum) + 2 * LON_MAX;

	if (diff < DIFF)
	{
		rightLonNum = leftLonNum + DIFF;
		if (LON_MAX < rightLonNum)
		{
			rightLonNum = LON_MIN + (rightLonNum - LON_MAX);
			alert("right Lon must be at least 5° more");
		}
	}
	rightLonE.value = rightLonNum;
	leftLonE.value = leftLonNum;
	return true;
}

function checkRightLon(rightLonE)
{
	var rightLon = rightLonE.value;
	var leftLonE = rightLonE.form.elements["gewocsRequest.minLon"];
	var leftLon = leftLonE.value;

	if ("" === rightLon)
	{
		alert("Please enter a number");
		rightLonE.value = "";
		return true;
	}
	if (isNaN(rightLon))
	{
		alert("Please enter a number");
		rightLonE.value = "";
		return true;
	}

	var rightLonNum = Number(rightLon);

	if (rightLonNum < LON_MIN)
	{
		alert("min Lon is " + LON_MIN);
		rightLonE.value = LON_MIN;
		leftLonE.value = LON_MAX - DIFF;
		return true;
	}

	if (LON_MAX < rightLonNum)
	{
		alert("max Lon is " + LON_MAX);
		rightLonE.value = LON_MAX;
		leftLonE.value = LON_MAX - DIFF;
		return true;
	}

	var leftLonNum = 0;
	if ("" === leftLon || isNaN(leftLon))
	{
		leftLonNum = rightLonNum - DIFF;
		if (leftLonNum < LON_MIN)
			leftLonNum = LON_MAX - (LON_MIN - leftLonNum);
	} else
		leftLonNum = Number(leftLon);

	var diff = 0;
	if (leftLonNum <= rightLonNum)
		diff = rightLonNum - leftLonNum;
	else if (rightLonNum < leftLonNum)
		diff = (rightLonNum - leftLonNum) + 2 * LON_MAX;

	if (diff < DIFF)
	{
		leftLonNum = rightLonNum - DIFF;
		if (leftLonNum < LON_MIN)
		{
			leftLonNum = LON_MAX - (LON_MIN - leftLonNum);
			alert("left Lon must be at least 5° less");
		}
	}
	rightLonE.value = rightLonNum;
	leftLonE.value = leftLonNum;
	return true;
}

function selectGlobe(button)
{
	var form = button.form;
	var leftLonE = form.elements["gewocsRequest.minLon"];
	var rightLonE = form.elements["gewocsRequest.maxLon"];
	var minLatE = form.elements["gewocsRequest.minLat"];
	var maxLatE = form.elements["gewocsRequest.maxLat"];
	leftLonE.value = LON_MIN;
	rightLonE.value = LON_MAX;
	minLatE.value = LAT_MIN;
	maxLatE.value = LAT_MAX;
}
