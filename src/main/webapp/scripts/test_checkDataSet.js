/* test_checkDataSet.js */

// initial value of oldDataSet
var oldDataSet = "";
function initDataSet() {
	var dataSetSelect = document
			.getElementById("gewocsFormId_gewocsRequest_dataSet");
	var selectedIndex = dataSetSelect.selectedIndex;
	var currentDataSet = dataSetSelect.options[selectedIndex].value;

	var startYearSelect = document
			.getElementById("gewocsFormId_gewocsRequest_startYear");
	var startYearIndex = startYearSelect.selectedIndex;
	var startYear = startYearSelect[startYearIndex].value;
	var endYearSelect = document
			.getElementById("gewocsFormId_gewocsRequest_endYear");
	var endYearIndex = endYearSelect.selectedIndex;
	var endYear = endYearSelect[endYearIndex].value;
	if (startYear == 0 || endYear == 0)

		initYears(currentDataSet, startYearSelect, endYearSelect);
	else
		checkDataSet(currentDataSet);

	oldDataSet = currentDataSet;
}

function initYears(dataSet, startYearSelect, endYearSelect) {
	var startYear = Number(startYearJSArray[dataSet]);
	var endYear = Number(endYearJSArray[dataSet]);
	startYearSelect.options.length = 0;
	endYearSelect.options.length = 0;
	var diff = endYear - startYear + 1;
	var year;
	var display;
	var value;
	for ( var i = 0; i < diff; i++) {
		year = i + startYear;
		display = year.toString();
		value = display;
		startYearSelect.options[i] = new Option(display, value,
				NOT_DEFAULT_SELECTED, NOT_SELECTED);
		endYearSelect.options[i] = new Option(display, value,
				NOT_DEFAULT_SELECTED, NOT_SELECTED);
	}
	startYearSelect.selectedIndex = 0;
	endYearSelect.selectedIndex = diff - 1;
}

function checkDataSet(dataSetSelect) {
	// alert('oldDataSet=' + oldDataSet);
	var dataSetIndex = dataSetSelect.selectedIndex;
	var newDataSet = dataSetSelect.options[dataSetIndex].value;
	var theForm = dataSetSelect.form;

	adjustYears(newDataSet, oldDataSet, theForm);
	adjuestSeason(newDataSet, theForm);

	oldDataSet = newDataSet;
	return true;
}

var minStartYear = 0;
var maxEndYear = 0;
var NOT_DEFAULT_SELECTED = false;
var NOT_SELECTED = false;
var DEFAULT_SELECTED = true;
var SELECTED = true;

function adjustYears(newDataSet, prevDataSet, theForm) {
	var newStartYear = Number(startYearJSArray[newDataSet]);
	// alert(newStartYear);
	var newEndYear = Number(endYearJSArray[newDataSet]);
	// alert(newEndYear);
	var prevStartYear = Number(startYearJSArray[prevDataSet]);
	// alert(prevStartYear);
	var prevEndYear = Number(endYearJSArray[prevDataSet]);
	// alert(prevEndYear);

	var startYearShift = newStartYear - prevStartYear;
	// alert(startYearShift);
	var endYearShift = newEndYear - prevEndYear;

	// Preserve the previous values
	var startYearSelect = theForm.elements["gewocsRequest.startYear"];
	var endYearSelect = theForm.elements["gewocsRequest.endYear"];
	var startYearSelectPreviousIndex = startYearSelect.selectedIndex;
	var endYearSelectPreviousIndex = endYearSelect.selectedIndex;
	var startYearPrevious = Number(startYearSelect[startYearSelectPreviousIndex].value);
	var endYearPrevious = Number(endYearSelect[endYearSelectPreviousIndex].value);

	// update the select options lists
	// If the previous array was longer, it has to be shortened.
	startYearSelect.options.length = 0;
	endYearSelect.options.length = 0;
	var diff = newEndYear - newStartYear + 1;
	// alert("diff="+diff);
	var year;
	var display;
	var value;
	for ( var i = 0; i < diff; i++) {
		year = i + newStartYear;
		display = year.toString();
		value = display;
		startYearSelect.options[i] = new Option(display, value,
				NOT_DEFAULT_SELECTED, NOT_SELECTED);
		endYearSelect.options[i] = new Option(display, value,
				NOT_DEFAULT_SELECTED, NOT_SELECTED);
	}

	// Now copy or adjust the year selections
	if (newStartYear < startYearPrevious && startYearPrevious < newEndYear)
		startYearSelect.selectedIndex = startYearSelectPreviousIndex
				- startYearShift;
	else if (startYearPrevious <= newStartYear) {
		startYearPrevious = newStartYear;
		startYearSelect.selectedIndex = 0;
	} else if (newEndYear <= startYearPrevious) {
		startYearSelect.selectedIndex = diff - 1;
		startYearPrevious = newEndYear;
	}

	if (startYearPrevious < endYearPrevious && endYearPrevious < newEndYear)
		endYearSelect.selectedIndex = endYearSelectPreviousIndex
				- startYearShift;
	else if (endYearPrevious <= startYearPrevious)
		endYearSelect.selectedIndex = startYearSelect.selectedIndex;
	else if (newEndYear <= endYearPrevious)
		endYearSelect.selectedIndex = diff - 1;
}

function adjuestSeason(dataSet, theForm) {
	var indexSelect = theForm.elements["gewocsRequest.climateIndex"];
	checkClimateIndex(indexSelect);
}