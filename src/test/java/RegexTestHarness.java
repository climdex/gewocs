
public class RegexTestHarness {

    public static void main(String[] args){
        

            String pattern = "\\$\\{INDEX\\}";
            String old = "test_${INDEX}_the_string";
            String replacement = "Txx";

           String newStr = old.replaceAll(pattern, replacement);
           
           System.out.println(newStr);
    }
}

