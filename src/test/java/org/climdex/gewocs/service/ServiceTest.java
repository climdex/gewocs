package org.climdex.gewocs.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.climdex.gewocs.service.dao.GewocsDAOImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * TODO: Add code to test HADEXs
 * TODO: Add code to test proper failures
 * @author yoichi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class ServiceTest
{
	private static final Logger logger = Logger.getLogger(ServiceTest.class);

	private static final String DELIM = File.separator;

	private static String gewocsResultDir = "/scratch/climdex/NCLScripts/gewocs/results";

	@Autowired
	private GewocsService gewocsService;

	public GewocsService getGewocsService()
	{
		return gewocsService;
	}

	public void setGewocsService(GewocsService gewocsService)
	{
		this.gewocsService = gewocsService;
	}

	/**
	 * Input bean for testing
	 */
	private GewocsInputDTO gewocsInputDTO;

	@Before
	public void prepare()
	{
		String dataSet = "GHCNDEX";
		String climateIndex = "TXx"; // choose from list of 27 indices
		String startYear = "1960"; // Data start 1951 - so do not offer earlier
									// years
		String endYear = "2000";
		String season = "ANN";
		String minLat = "-30"; // select sub-regions minLat-maxLat,
								// minLon-maxLon
		String maxLat = "80"; // Lat-values (-90 - 90), minLat<maxLat!!
		String minLon = "-80"; // Lon-Values (-180 - 180), minLon<maxLon!!
		String maxLon = "180";
		String outputType = GewocsDAOImpl.OUTPUT_TREND_MAP; 
		// choose from "Average", "Trend", "TimeSeries", "RawData"
		String outputFormat = GewocsDAOImpl.OUTPUT_PNG;
		// choose from PNG, netCDF, ASCII

		this.gewocsInputDTO = new GewocsInputDTO();
		this.gewocsInputDTO.setDataSet(dataSet);
		this.gewocsInputDTO.setClimateIndex(climateIndex);
		this.gewocsInputDTO.setStartYear(startYear);
		this.gewocsInputDTO.setEndYear(endYear);
		this.gewocsInputDTO.setSeason(season);
		this.gewocsInputDTO.setMinLat(minLat);
		this.gewocsInputDTO.setMaxLat(maxLat);
		this.gewocsInputDTO.setMinLon(minLon);
		this.gewocsInputDTO.setMaxLon(maxLon);
		this.gewocsInputDTO.setOutputType(outputType);
		this.gewocsInputDTO.setOutputFormat(outputFormat);
	}

	@Test
	public void testTrendPNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	@Test
	public void testTrendNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testTrendASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testAveragePNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	@Test
	public void testAverageNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testAverageASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testTimeSeriesPNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	// TimeSeries does not produce NetCDF

	@Test
	public void testTimeSeriesASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}
	
	// RawData does not produce PNG
	
	@Test
	public void testRawDataNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testRawDataASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	/**
	 * DAO does not create the source file for the InputStream, if it has been
	 * already created. Delete the source file, if it should be tested. This
	 * code deletes the copy of the file created from the InputSream.
	 */
	public void execute()
	{
		assertTrue(this.gewocsService != null);
		assertTrue(this.gewocsInputDTO != null);

		GewocsOutputDTO gewocsOutputDTO = null;
		InputStream fis = null;
		OutputStream os = null;
		try
		{
			// Run the NCL script
			gewocsOutputDTO = this.gewocsService
					.getGewocsOutput(this.gewocsInputDTO);

			fis = gewocsOutputDTO.getFileInputStream();
			String fileName = gewocsOutputDTO.getFileName();

			// needs to make a file path because neither DTOs know it.
			String outFilePath = gewocsResultDir + DELIM + fileName;
			String outFilePath2 = gewocsResultDir + DELIM + "test_" + fileName;

			File outFile = new File(outFilePath);
			if (!outFile.exists())
				throw new ServiceException(fileName + ": does not exist");

			File outFile2 = new File(outFilePath2);
			if (outFile2.exists()) outFile2.delete();

			// Just copy the file to another file
			os = new FileOutputStream(outFile2);
			byte buf[] = new byte[1024];
			int len;
			while ((len = fis.read(buf)) > 0)
				os.write(buf, 0, len);
		}
		catch (ServiceException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		finally
		{
			try
			{
				if (os != null) os.close();
				if (fis != null) fis.close();
			}
			catch (IOException e)
			{
				// ignored
			}
		}
	}
}
