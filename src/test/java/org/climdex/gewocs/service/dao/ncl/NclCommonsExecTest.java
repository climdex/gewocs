/*
 * Copyright 2010-2012 CLIMDEX Project Group www.climdex.org. Licensed under the
 * Educational Community License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 * http://www.osedu.org/licenses/ECL-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * CLIMDEX Project Group provides access to this code for bona fide scientific
 * research and personal usage only.
 * 
 * Please note that this is not an operational service. The project is run by
 * research staff with the primary aim of improving collaboration with fellow
 * researchers. Everyone is welcome to use the code, and we aim to make it
 * reliable, but it does not have the same level of support as an operational
 * service.
 */
package org.climdex.gewocs.service.dao.ncl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.climdex.gewocs.service.dao.DAOException;
import org.climdex.gewocs.service.dao.GewocsDAOImpl;
import org.climdex.gewocs.service.dao.GewocsNclDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 
 * <p>
 * NclCommonsExecTest.java
 * </p>
 * 
 * {@link http
 * ://static.springsource.org/spring/docs/2.5.x/reference/testing.html} Spring
 * 2.5.x Testing
 * TODO: This code is too old. Add code to test HADEXs
 * @author yoichi
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class NclCommonsExecTest
{
	private static final String DELIM = File.separator;

	// This is not supposed to be Autowired, but, without Autowired, it does not work.
	@Autowired
	private NclCommonsExec nclCommonsExec = null;
	private String dataDir = "/scratch/climdex/ghcndex/GHCNDEX_2012/ghcndex_Jul2012/gridout/GHCNDEX_2.5x2.5deg";
	private String scriptPath = "/scratch/climdex/NCLScripts/gewocs/plot6.ncl";
	private String gewocsResultDir = "/scratch/climdex/NCLScripts/gewocs/results";

	private String dataSet = "GHCNDEX";
	private String climateIndex = "TXx";
	private String startYear = "1960";
	private String endYear = "2010";
	private String season = "ANN";
	private String minLat = "0";
	private String maxLat = "50";
	private String minLon = "50";
	private String maxLon = "180";
	private String outputType = ""; // TrendMap, TimeSeries, AverageMap, RawData
	private String outputFormat = ""; // PNG, NetCDF, ASCII
	
	private String dataFilePath = "";
	private String outFilePath = "";
	
	private GewocsNclDTO gewocsNclDTO = null;

	@Before
	public void prepareConstants()
	{
		this.gewocsNclDTO = new GewocsNclDTO();
		this.gewocsNclDTO.setDataSet(this.dataSet);
		this.gewocsNclDTO.setClimateIndex(this.climateIndex); // can be different values
		this.gewocsNclDTO.setStartYear(this.startYear);
		this.gewocsNclDTO.setEndYear(this.endYear);
		this.gewocsNclDTO.setSeason(this.season);
		this.gewocsNclDTO.setMinLat(this.minLat);
		this.gewocsNclDTO.setMaxLat(this.maxLat);
		this.gewocsNclDTO.setMinLon(this.minLon);
		this.gewocsNclDTO.setMaxLon(this.maxLon);
		this.gewocsNclDTO.setOutputType("");
		this.gewocsNclDTO.setOutputFormat("");		
		this.gewocsNclDTO.setScriptPath(this.scriptPath);
	}

	@Test
	public void testAverageMapPNG() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	@Test
	public void testAverageMapNetCDF() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testAverageMapASCII() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testTrendMapPNG() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	@Test
	public void testTrendMapNetCDF() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testTrendMapASCII() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testTimseSeriesPNG() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}
	
	// No NetNCD for TimseSeries

	@Test
	public void testTimseSeriesASCII() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}
	
	// No PNG for RawData

	@Test
	public void testRawDataNetCDF() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testRawDataASCII() throws IOException
	{
		this.gewocsNclDTO.setClimateIndex(climateIndex);
		this.gewocsNclDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsNclDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	private void execute()
	{
		assertTrue(this.nclCommonsExec != null);
		
		File outFile = null;
		try
		{
			this.climateIndex = this.gewocsNclDTO.getClimateIndex();
			this.dataFilePath = dataDir + DELIM + "GHCND_" + climateIndex
					+ "_1951-2012_RegularGrid_global_2.5x2.5deg_LSmask.nc";
			gewocsNclDTO.setDataFilePath(this.dataFilePath);

			String fileName = makeFileName();			
			this.outFilePath = this.gewocsResultDir + DELIM + fileName;
			gewocsNclDTO.setOutFilePath(this.outFilePath);

			outFile = new File(this.outFilePath);
			if (outFile.exists()) outFile.delete();

			this.nclCommonsExec.execute(gewocsNclDTO);
			if (!outFile.exists())
				throw new NclCommonsExecException(fileName + ": not created");
		}
		catch (NclCommonsExecException e)
		{
			fail(e.getMessage());
		}
		catch (DAOException e)
		{
			fail(e.getMessage());
		}
	}
	
	private String makeFileName() throws DAOException
	{
		String dataSet = this.gewocsNclDTO.getDataSet();
		String climateIndex = this.gewocsNclDTO.getClimateIndex();
		String startYear = this.gewocsNclDTO.getStartYear();
		String endYear = this.gewocsNclDTO.getEndYear();
		String season = this.gewocsNclDTO.getSeason();
		String minLat = this.gewocsNclDTO.getMinLat();
		String maxLat = this.gewocsNclDTO.getMaxLat();
		String minLon = this.gewocsNclDTO.getMinLon();
		String maxLon = this.gewocsNclDTO.getMaxLon();
		String outputType = this.gewocsNclDTO.getOutputType();
		String outputFormat = this.gewocsNclDTO.getOutputFormat();
		
		String suffix = "";
		if (GewocsDAOImpl.OUTPUT_ASCII.equals(outputFormat))
		{
			suffix = GewocsDAOImpl.SUFFIX_ASCII;
		}
		else if (GewocsDAOImpl.OUTPUT_NETCDF.equals(outputFormat))
		{
			suffix = GewocsDAOImpl.SUFFIX_NETCDF;
		}
		else if (GewocsDAOImpl.OUTPUT_PNG.equals(outputFormat))
		{
			suffix = GewocsDAOImpl.SUFFIX_PNG;
		}
		else
			throw new DAOException(outputType + ": unknown outputType");

		String fileName = outputType + "_" + dataSet + "_" + climateIndex + "_"
				+ startYear + "-" + endYear + "_" + season + "_from" + minLat
				+ "to" + maxLat + "_from" + minLon + "to" + maxLon + "."
				+ suffix;
			
		return fileName;
	}
	
	public NclCommonsExec getNclCommonsExec()
	{
		return nclCommonsExec;
	}

	public void setNclCommonsExec(NclCommonsExec nclCommonsExec)
	{
		this.nclCommonsExec = nclCommonsExec;
	}

	public String getDataDir()
	{
		return dataDir;
	}

	public void setDataDir(String dataDir)
	{
		this.dataDir = dataDir;
	}

	public String getScriptPath()
	{
		return scriptPath;
	}

	public void setScriptPath(String scriptPath)
	{
		this.scriptPath = scriptPath;
	}

	public String getGewocsResultDir()
	{
		return gewocsResultDir;
	}

	public void setGewocsResultDir(String gewocsResultDir)
	{
		this.gewocsResultDir = gewocsResultDir;
	}
}
