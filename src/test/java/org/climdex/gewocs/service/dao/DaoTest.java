package org.climdex.gewocs.service.dao;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.climdex.gewocs.action.GewocsAction;
import org.climdex.gewocs.service.GewocsInputDTO;
import org.climdex.gewocs.service.GewocsOutputDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * TODO: Add tests for HADEXx
 * @author yoichi
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class DaoTest
{
	private static final Logger logger = Logger.getLogger(GewocsAction.class);

	@Autowired
	private GewocsDAO gewocsDAO;

	public GewocsDAO getGewocsDAO()
	{
		return gewocsDAO;
	}

	public void setGewocsDAO(GewocsDAO gewocsDAO)
	{
		this.gewocsDAO = gewocsDAO;
	}

	/**
	 * Input bean for testing
	 */
	private GewocsInputDTO gewocsInputDTO;

	@Before
	public void prepare()
	{
		String dataSet = "GHCNDEX";
		String climateIndex = "TXx"; // choose from list of 27 indices
		String startYear = "1960"; // Data start 1951 - so do not offer earlier
									// years
		String endYear = "2000";
		String season = "ANN";
		String minLat = "-30"; // select sub-regions minLat-maxLat,
								// minLon-maxLon
		String maxLat = "80"; // Lat-values (-90 - 90), minLat<maxLat!!
		String minLon = "-80"; // Lon-Values (-180 - 180), minLon<maxLon!!
		String maxLon = "180";
		String outputType = ""; 
		// choose from "Average", "Trend", "TimeSeries", "RawData"
		String outputFormat = "";
		// choose from PNG, netCDF, ASCII

		this.gewocsInputDTO = new GewocsInputDTO();
		this.gewocsInputDTO.setDataSet(dataSet);
		this.gewocsInputDTO.setClimateIndex(climateIndex);
		this.gewocsInputDTO.setStartYear(startYear);
		this.gewocsInputDTO.setEndYear(endYear);
		this.gewocsInputDTO.setSeason(season);
		this.gewocsInputDTO.setMinLat(minLat);
		this.gewocsInputDTO.setMaxLat(maxLat);
		this.gewocsInputDTO.setMinLon(minLon);
		this.gewocsInputDTO.setMaxLon(maxLon);
		this.gewocsInputDTO.setOutputType(outputType);
		this.gewocsInputDTO.setOutputFormat(outputFormat);
	}

	@Test
	public void testTrendPNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}

	@Test
	public void testTrendNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testTrendASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TREND_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testAveragePNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}
	
	@Test
	public void testAverageNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}
	
	@Test
	public void testAverageASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_AVERAGE_MAP);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	@Test
	public void testTimeSeriesPNG() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_PNG);
		execute();
	}
	
	// TimseSeries does not produce NetCDF output

	@Test
	public void testTimeSeriesASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_TIME_SERIES);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}
	
	// RawData does not produce PNG output

	@Test
	public void testRawDataNetCDF() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_NETCDF);
		execute();
	}

	@Test
	public void testRawDataASCII() throws IOException
	{
		this.gewocsInputDTO.setClimateIndex("TXx");
		this.gewocsInputDTO.setOutputType(GewocsDAOImpl.OUTPUT_RAW_DATA);
		this.gewocsInputDTO.setOutputFormat(GewocsDAOImpl.OUTPUT_ASCII);
		execute();
	}

	/**
	 * DAO does not create an output file if it exists. For this reason, this
	 * code deletes the file beforehand
	 */
	public void execute()
	{
		assertTrue(this.gewocsDAO != null);
		assertTrue(this.gewocsInputDTO != null);

		GewocsDAOImpl dao = (GewocsDAOImpl) this.gewocsDAO;
		GewocsOutputDTO gewocsOutputDTO = null;
		GewocsNclDTO gewocsNclDTO = null;
		InputStream fis = null;
		OutputStream os = null;
		try
		{
			// Run the DAO
			// gewocsNclDTO is READ-ONLY as it will be re-created by
			// getGewocsOutput()
			gewocsNclDTO = dao.makeNclDTO(gewocsInputDTO);

			String dataSet = gewocsNclDTO.getClimateIndex();
			String climateIndex = gewocsNclDTO.getClimateIndex();
			String startYear = gewocsNclDTO.getStartYear();
			String endYear = gewocsNclDTO.getEndYear();
			String season = gewocsNclDTO.getSeason();
			String minLat = gewocsNclDTO.getMinLat();
			String maxLat = gewocsNclDTO.getMaxLat();
			String minLon = gewocsNclDTO.getMinLon();
			String maxLon = gewocsNclDTO.getMaxLon();
			String outputType = gewocsNclDTO.getOutputType();
			String outputFormat = gewocsNclDTO.getOutputFormat();;

			String fileName = gewocsNclDTO.getFileName();
			String contentType = gewocsNclDTO.getContentType();

			String dataFilePath = gewocsNclDTO.getDataFilePath();
			String outFilePath = gewocsNclDTO.getOutFilePath();
			String scriptPath = gewocsNclDTO.getScriptPath();

			logger.debug("dataSet= " + dataSet);
			logger.debug("climateIndex= " + climateIndex);
			logger.debug("startYear= " + startYear);
			logger.debug("endYear= " + endYear);
			logger.debug("season= " + season);
			logger.debug("minLat= " + minLat);
			logger.debug("maxLat= " + maxLat);
			logger.debug("minLon= " + minLon);
			logger.debug("maxLon= " + maxLon);
			logger.debug("outputType= " + outputType);
			logger.debug("contentType= " + contentType);
			logger.debug("outputFormat= " + outputFormat);
			logger.debug("fileName= " + fileName);
			logger.debug("outFilePath= " + outFilePath);
			logger.debug("dataFilePath= " + dataFilePath);
			logger.debug("scriptPath= " + scriptPath);

			File outFile = new File(outFilePath);
			// Just need to delete the file, if it exists, for assertion
			if (outFile.exists()) outFile.delete();

			gewocsOutputDTO = dao.getGewocsOutput(this.gewocsInputDTO);

			fis = gewocsOutputDTO.getFileInputStream();
			if (fis == null) fail("fileInputStream is null");

			String gewocsResultDir = dao.getGewocsResultDir();
			String outFilePath2 = gewocsResultDir +"/daoTest_" + fileName;

			outFile = new File(outFilePath2);
			if (outFile.exists()) outFile.delete();

			os = new FileOutputStream(outFile);
			byte buf[] = new byte[1024];
			int len;
			while ((len = fis.read(buf)) > 0)
				os.write(buf, 0, len);
		}
		catch (DAOException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		finally
		{
			try
			{
				if (os != null) os.close();
				if (fis != null) fis.close();
			}
			catch (IOException e)
			{
				// ignored
			}
		}
	}
}
